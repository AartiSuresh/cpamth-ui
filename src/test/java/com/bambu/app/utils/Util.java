package com.bambu.app.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class Util {

	private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);
	private static Util instance;
	private static EnvironmentVariables variables;
	static Connection c = null;
	static Statement stmt = null;
	static String postgresUname = "root";
	static String postgresPwd = "Bambu2019";
	static String postgresHost = "jdbc:postgresql://bca-dev.c1rtd5yunjqo.us-east-1.rds.amazonaws.com:5432/";
	static ResultSet rs;

	public synchronized static Util get() {
		if (instance == null) {
			instance = new Util();
			variables = SystemEnvironmentVariables.createEnvironmentVariables();
		}
		return instance;
	}

	public String getSerenityProperty(String name) {
		return variables.getProperty(name);
	}

	public static void getPostGresConnection(String database) {

		try {
			String connString = postgresHost + database;
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection(connString, postgresUname, postgresPwd);
			c.setAutoCommit(false);
			System.out.println("Opened database successfully");

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

	}

	public static void getResultSet(String query) {
		try {
			
			stmt = c.createStatement();
			rs = stmt.executeQuery(query);

		}

		catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Operation done successfully");

	}

	public static void getHouseData() throws SQLException {
		try {
			String region;
			String city;
			String houseType;
			String price;
			List<String> housedata=new ArrayList<String>();
			Map<String,String> housegoalData=new HashMap<String,String>();
			while (rs.next()) {
				region = rs.getString("region");
				city=rs.getString("city");
				houseType=rs.getString("houseType");
				price=rs.getString("price");
				housedata.add(region+"~"+city+"~"+houseType+"~"+price);
				housegoalData.put(region+"~"+city+"~"+houseType, price);
				System.out.println("|"+region+"|"+city+"|"+houseType+"|"+price+"|");
				
			}
			System.out.println("Total records="+housedata.size());
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			rs.close();
			stmt.close();
			c.close();
		}
	}

}
