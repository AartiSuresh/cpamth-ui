package com.bambu.app.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bambu.app.testcases.AdminConsoleTest;
//import com.bambu.app.testcases.BCA_Test;

public class SelUtil {
	
		
	
	public static String getPageTitle()
	{
		return AdminConsoleTest.wd.getTitle();
	}
		
	public static String getPageUrl()
	{
		return AdminConsoleTest.wd.getCurrentUrl();
	}
	
	public static void navigateBack() {
		AdminConsoleTest.wd.navigate().back();
		
	}
	
	public static void navigateTo(String url) {
		// TODO Auto-generated method stub
		AdminConsoleTest.wd.navigate().to(url);
	}
	
	public static void waitForPageLoad() {
	    new WebDriverWait(AdminConsoleTest.wd, 120)
	         .until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='loaderStyles']")));
	}
	
	public static void waitForElementNotPresent(WebElement xpathelement,Integer timeoutsecs) {
	    new WebDriverWait(AdminConsoleTest.wd, timeoutsecs)
	         .until(ExpectedConditions.invisibilityOfElementLocated((By) xpathelement));
	}
	
	public static void waitForElementPresent(By xpathelement,Integer timeoutsecs) {
	    new WebDriverWait(AdminConsoleTest.wd, timeoutsecs)
	         .until(ExpectedConditions.presenceOfElementLocated(xpathelement));
	}
	
	public static boolean SearchElementThroughPages(String strTexttoSearch,String strNoDatatoDisplay)
	{	
		int intPageCounter=1;
		//WebElement objUIControl= null;
		boolean blnPresent = false;		
			// While element is not present, loop
			waitForPageLoad();
			while(!blnPresent)
			{
				try 
				{
					AdminConsoleTest.wd.findElement(By.xpath(strTexttoSearch)).isDisplayed();					
					//Element is present
					blnPresent = true;
				} 
				catch (NoSuchElementException e) 
				{
					try 
					{
						AdminConsoleTest.wd.findElement(By.xpath("//*[text()='"+strNoDatatoDisplay+"']")).isDisplayed();
						blnPresent = false;
						e.printStackTrace();
						break;
					}
					catch(NoSuchElementException e1)
					{
						// Click on next page
						AdminConsoleTest.wd.findElement(By.linkText(String.valueOf(intPageCounter+1))).click();
						waitForPageLoad();
						intPageCounter = intPageCounter+1;
					}
					catch (Exception e2)
					{
						// Some other exception - unexpected
						e2.printStackTrace();
						blnPresent = false;
					}					
				}
				catch (Exception e3)
				{
					// Some other exception - unexpected
					e3.printStackTrace();
					blnPresent = false;
				}
			}
		// returns status of the element presence	
		return blnPresent; 		
				
	}

}
