package com.bambu.app.pages;

import org.openqa.selenium.NoSuchElementException;

import com.bambu.app.utils.SelUtil;

import org.junit.Assert;
import net.serenitybdd.core.pages.PageObject;

public class DashboardPage extends PageObject{
	String xpathLnk_Clients = "//a//span[text()='clients']";
	String xpathLnk_Logout = "//button[@text='LOGOUT']";
	String xpathTxt_Search = "//input[@type='text']";
	
	
	public String pageUrl = "http://dev.cpam-thai-dashboard.s3-website-ap-southeast-1.amazonaws.com/";
	
	public void clickClientLink() {
		$(xpathLnk_Clients).click();
		SelUtil.waitForPageLoad();
	}
	
	public void clickLogoutLink() {
		$(xpathLnk_Logout).click();
	}
	
	public void searchClient(String cname) 
	{
		SelUtil.waitForPageLoad();
		$(xpathTxt_Search).clear();
		$(xpathTxt_Search).sendKeys(cname);
		// code to wait until required element appears and then return true or false for presence of element
	}
	
	public void searchClientThroughPages(String cname, String strNodataDisplayText)
	{		
		String xpathLnk_ClientName = "//button[text()='"+cname+"']";
		boolean isClientPresent = SelUtil.SearchElementThroughPages(xpathLnk_ClientName, strNodataDisplayText);
		Assert.assertEquals("The searched client is not present.",true,isClientPresent);
	}
	
	public String getPageURL() {
		return SelUtil.getPageUrl();
	}

	public void clickonClientAfterSearch(String cname) {		
		searchClient(cname);
		String xpathLnk_ClientName = "//button[text()='"+cname+"']";
		$(xpathLnk_ClientName).click();				
	}
	
	public void verifyDocStatus(String expStatus,String ClientName)
	{		
		String xpath_DocStatus = "(//button[text()='"+ClientName+"']/../../td/a/span)[1]";
		String actualDocStatus = $(xpath_DocStatus).getText();
		expStatus = expStatus.toLowerCase();
		actualDocStatus = actualDocStatus.toLowerCase();
		Assert.assertEquals("Document Status for client is incorrect",expStatus,actualDocStatus);
	}
	public void verifyAccStatus(String expStatus,String ClientName)
	{
		String xpath_AccStatus = "(//button[text()='"+ClientName+"']/../../td/a/span)[2]";
		String actualAccStatus = $(xpath_AccStatus).getText();
		expStatus = expStatus.toLowerCase();
		actualAccStatus = actualAccStatus.toLowerCase();
		Assert.assertEquals("Account Status for client is incorrect",expStatus,actualAccStatus);
	}
	
}
