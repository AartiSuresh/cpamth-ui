package com.bambu.app.pages;

import com.bambu.app.utils.SelUtil;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://dev.cpam-thai-dashboard.s3-website-ap-southeast-1.amazonaws.com/login")
public class AdminLoginPage extends PageObject{
	
	String xpathBtn_login = "//button[@class='btnProj login']";
	String xpathTxt_username = "//input[@name='email']";
	String xpathTxt_password = "//input[@name='password']";
	
	public static String loginPagetitle = "CPAM";
	
	public void enterCredentials(String uname, String pwd) {
		System.out.println("\""+uname+"="+pwd+"\"");
		$(xpathTxt_username).sendKeys(uname);
		$(xpathTxt_password).sendKeys(pwd);
	}
	
	public void clickLogin() {
		$(xpathBtn_login).click();
	}
	
	public String getPageTitle() {
		return SelUtil.getPageTitle();
	}
	
	public void navigateTo(String url) {
		SelUtil.navigateTo(url);
	}

}
