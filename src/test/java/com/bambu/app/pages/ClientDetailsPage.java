package com.bambu.app.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.bambu.app.utils.SelUtil;

import net.serenitybdd.core.pages.PageObject;

public class ClientDetailsPage extends PageObject{
	
	//String pageUrl = "http://dev.cpam-thai-dashboard.s3-website-ap-southeast-1.amazonaws.com/"; 
	
	String xpathTab_Documents = "//span[text()='DOCUMENTS']";
	String xpathTab_ClientProfile = "//span[text()='PROFILE']";
	//Thai documents
	// ID Front
	String xpathLnk_IDFront = "//a[text()='Identification front']";
	String xpathLnk_ApproveIDFront = "//a[text()='Identification front']//following::a[text()='Approve']";
	String xpathLnk_RejectIDFront = "//a[text()='Identification front']//following::a[text()='Reject']";
	String xpathImg_ApproveIDFront_Approved = "//a[text()='Identification front']//following::span[text()='Approved']";
	String xpathImg_ApproveIDFront_Rejected = "//a[text()='Identification front']//following::span[text()='Rejected']";
	// ID Back
	String xpathLnk_IDBack = "//a[text()='Identification back']";
	String xpathLnk_ApproveIDBack = "//a[text()='Identification back']//following::a[text()='Approve']";
	String xpathLnk_RejectIDBack = "//a[text()='Identification back']//following::a[text()='Reject']";
	String xpathImg_ApproveIDBack_Approved = "//a[text()='Identification back']//following::span[text()='Approved']";
	String xpathImg_ApproveIDBack_Rejected = "//a[text()='Identification back']//following::span[text()='Rejected']";
	 
	// Income Statement
	String xpathLnk_IncomeStatement = "//a[text()='Income statement']";
	String xpathLnk_ApproveIncomeSt = "//a[text()='Income statement']//following::a[text()='Approve']";
	String xpathLnk_RejectIncomeSt = "//a[text()='Income statement']//following::a[text()='Reject']";
	String xpathImg_ApproveIncomeSt_Approved = "//a[text()='Income statement']//following::span[text()='Approved']";
	String xpathImg_ApproveIncomeSt_Rejected = "//a[text()='Income statement']//following::span[text()='Rejected']";
	
	// Bankbook
	String xpathLnk_Bankbook = "//a[text()='Bankbook']";
	String xpathLnk_ApproveBankbook = "//a[text()='Bankbook']//following::a[text()='Approve']";
	String xpathLnk_RejectBankbook = "//a[text()='Bankbook']//following::a[text()='Reject']";
	String xpathImg_ApproveBankbook_Approved = "//a[text()='Bankbook']//following::span[text()='Approved']";
	String xpathImg_ApproveBankbook_Rejected = "//a[text()='Bankbook']//following::span[text()='Rejected']";
	
	// Foreigner Documents
	String xpathLnk_PassportFront = "//a[text()='Passport front']";
	String xpathLnk_ApprovePasportFront = "//a[text()='Passport front']//following::a[text()='Approve']";
	String xpathLnk_RejectPassportFront = "//a[text()='Passport front']//following::a[text()='Reject']";
	String xpathImg_ApprovePassportFront_Approved = "//a[text()='Passport front']//following::span[text()='Approved']";
	String xpathImg_ApprovePassportFront_Rejected = "//a[text()='Passport front']//following::span[text()='Rejected']";
	
	String xpathBtn_ApproveKYC = "//button[text()='APPROVE KYC']";
	String xpathBtn_RejectKYC = "//button[text()='REJECT CLIENT']";
	String xpathBtn_OK = "//button[text()='YES']";
	String xpathBtn_NO = "//button[text()='NO']";
	String xpathImg_ClientKYC_Pending = "//div[@class='status']//span[text()='Pending']";
	String xpathImg_ClientKYC_Approved = "//div[@class='status']//span[text()='Approved']";
	String xpathImg_ClientKYC_Rejected = "//div[@class='status']//span[text()='Rejected']";
	
	public void clickDocumentsTab() {
		$(xpathTab_Documents).click();
			}
	
	public void clickClientsTab() {
		$(xpathTab_ClientProfile).click();
			}	

	// THAI documents 
	public void approveIDFront() {		
		$(xpathLnk_ApproveIDFront).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Approved status is not displayed",$(xpathImg_ApproveIDFront_Approved).isDisplayed(),true);
	}
	
	public void rejectIDFront() {		
		$(xpathLnk_RejectIDFront).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Rejected status is not displayed",$(xpathImg_ApproveIDFront_Rejected).isDisplayed(),true);
	}
	
	public void approveIDBack() {		
		$(xpathLnk_ApproveIDBack).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Approved status is not displayed",$(xpathImg_ApproveIDBack_Approved).isDisplayed(),true);
	}
	
	public void rejectIDBack() {		
		$(xpathLnk_RejectIDBack).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Rejected status is not displayed",$(xpathImg_ApproveIDBack_Rejected).isDisplayed(),true);
	}
	
	public void approveIncomeSt() {		
		$(xpathLnk_ApproveIncomeSt).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Approved status is not displayed",$(xpathImg_ApproveIncomeSt_Approved).isDisplayed(),true);
	}
	
	public void rejectIncomeSt() {		
		$(xpathLnk_RejectIncomeSt).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Rejected status is not displayed",$(xpathImg_ApproveIncomeSt_Rejected).isDisplayed(),true);
	}
	
	public void approveBankBook() {		
		$(xpathLnk_ApproveBankbook).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Approved status is not displayed",$(xpathImg_ApproveBankbook_Approved).isDisplayed(),true);
	}
	
	public void rejectBankBook() {		
		$(xpathLnk_RejectBankbook).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Rejected status is not displayed",$(xpathImg_ApproveBankbook_Rejected).isDisplayed(),true);
	}
	
	// Foreigner doc 
	
	public void approvePassportFront() {		
		$(xpathLnk_ApprovePasportFront).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Approved status is not displayed",$(xpathImg_ApprovePassportFront_Approved).isDisplayed(),true);
	}
	
	public void rejectPassportFront() {		
		$(xpathLnk_RejectPassportFront).click();
		SelUtil.waitForPageLoad();
		Assert.assertEquals("Rejected status is not displayed",$(xpathImg_ApprovePassportFront_Rejected).isDisplayed(),true);
	}
	
	public boolean isClientDisplayed(String cname)
	{			
		String xpath_ClientNameHeader = "//span[text()='"+cname+"']";
		//SelUtil.waitforVisibility($(xpath_ClientNameHeader));
		SelUtil.waitForPageLoad();
		return $(xpath_ClientNameHeader).isDisplayed();
	}
	
	public void approveClientKYC()
	{
		$(xpathBtn_ApproveKYC).click();
		$(xpathBtn_OK).click();
		SelUtil.waitForPageLoad();
		SelUtil.waitForElementPresent((By.xpath(xpathImg_ClientKYC_Approved)), 15);
		//SelUtil.waitForElementNotPresent(($(xpathImg_ClientKYC_Pending)), 10);
		Assert.assertEquals("Approve KYC failed.",$(xpathImg_ClientKYC_Approved).isDisplayed(),true);
	}
	
	public void rejectClientKYC()
	{
		$(xpathBtn_RejectKYC).click();
		$(xpathBtn_OK).click();
		SelUtil.waitForPageLoad();
		SelUtil.waitForElementPresent((By.xpath(xpathImg_ClientKYC_Rejected)), 15);
		//SelUtil.waitForElementNotPresent(($(xpathImg_ClientKYC_Pending)), 10);
		Assert.assertEquals("Approve KYC failed.",$(xpathImg_ClientKYC_Rejected).isDisplayed(),true);
	}
	
	
	public void approveAllThaiDocuments()
	{
		approveIDFront();
		approveIDBack();
		approveIncomeSt();
		approveBankBook();
	}
	
	public void rejectAllThaiDocuments()
	{
		rejectIDFront();
		rejectIDBack();
		rejectIncomeSt();
		rejectBankBook();
	}

	public void approveAllForeignerDocuments()
	{
		approvePassportFront();		
		approveIncomeSt();		
	}
	
	public void rejectAllForeignerDocuments()
	{
		rejectPassportFront();		
		rejectIncomeSt();		
	}
	
	
	
	
	
}
