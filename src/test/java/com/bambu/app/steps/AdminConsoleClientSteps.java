package com.bambu.app.steps;



import org.junit.Assert;

import com.bambu.app.pages.AdminLoginPage;
import com.bambu.app.pages.ClientDetailsPage;
import com.bambu.app.pages.DashboardPage;
import com.bambu.app.testcases.AdminConsoleTest;
import com.bambu.app.utils.SelUtil;

import net.thucydides.core.annotations.Step;

public class AdminConsoleClientSteps {
	
	AdminLoginPage AdminLogin;
	DashboardPage Dashboard;
	ClientDetailsPage ClientDetails;
	
	@Step("Admin Console login page is launched")
	public void onLoginPage() {
		AdminLogin.open();
		AdminConsoleTest.wd.manage().window().maximize();
	}	
	
	@Step("Enter Admin uname:{0},pwd:{1} and click on Login")
	public void logintoClientList(String uname, String pwd) {
		AdminLogin.enterCredentials(uname, pwd);
		AdminLogin.clickLogin();
	}	
	
	@Step("Verifying loggedin page URL")
	public void verifyPageTitle() throws InterruptedException {

		String actualPageURL = Dashboard.getPageURL();
		String expectedPageURL = Dashboard.pageUrl;
		Assert.assertEquals("Page title is not matching", expectedPageURL, actualPageURL);

	}	
	
	@Step("Admin searched for Client:{0}")
	public void searchClient(String cname) {
		// TODO Auto-generated method stub
		Dashboard.searchClient(cname);
	}
	
	@Step("Admin searched for Client:{0} through pages")
	public void searchClientThroughPages(String cname) {
		Dashboard.searchClientThroughPages(cname,"There is no data to display");
	}
	
	@Step("Admin navigates to Client Profile of Client:{0}")
	public void navigatetoClientProfile(String cname) {
		// TODO Auto-generated method stub
		Dashboard.clickonClientAfterSearch(cname);
	}
	
	@Step("Verifying Client:{0} details displayed")
	public void verifyClientDetailsDisplayed(String cname) {		
		boolean isClientDisp = ClientDetails.isClientDisplayed(cname);
		Assert.assertEquals("Verify Client details are displayed",isClientDisp, true);
	}
	
	@Step("Navigate to Clients page")
	public void navigatetoClientsPage() {
		Dashboard.clickClientLink();		
	}
	
	@Step("Navigate to Documents tab")
	public void clickonDocuments() {
		ClientDetails.clickDocumentsTab();
	}
	
	@Step("Approve all documents for Thai client")
	public void approveAllThaiDocuments()
	{
		ClientDetails.approveAllThaiDocuments();
	}
	
	@Step("Reject all documents for Thai client")
	public void rejectAllThaiDocuments()
	{
		ClientDetails.rejectAllThaiDocuments();
	}
	
	@Step("Approve all documents for Foreigner client")
	public void approveAllForeignerDocuments()
	{
		ClientDetails.approveAllForeignerDocuments();
	}
	
	@Step("Reject all documents for Foreigner client")
	public void rejectAllForeignerDocuments()
	{
		ClientDetails.rejectAllForeignerDocuments();
	}
		
	@Step("Click on Client Profile details tab")
	public void clickProfileTab()
	{
		ClientDetails.clickClientsTab();
	}
	
	@Step("Approve Client KYC")
	public void approveKYC()
	{
		ClientDetails.approveClientKYC();
	}
	
	@Step("Reject Client KYC")
	public void rejectKYC()
	{
		ClientDetails.rejectClientKYC();
	}
	
	@Step("Validate approved Document Status of Client {0}")
	public void validateApprovedDocStatus(String cname)
	{
		Dashboard.verifyDocStatus("Approved", cname);
	}
	
	
	@Step("Validate rejected Document Status of Client {0}")
	public void validateRejectedDocStatus(String cname)
	{
		Dashboard.verifyDocStatus("Rejected", cname);
	}
	
	@Step("Validate approved Account Status of Client {0}")
	public void validateApprovedAccStatus(String cname)
	{
		Dashboard.verifyAccStatus("Approved", cname);
	}
	
	@Step("Validate rejected Account Status of Client {0}")
	public void validateRejectedAccStatus(String cname)
	{
		Dashboard.verifyAccStatus("Rejected", cname);
	}
	
	@Step("Validate 'Pending' Account Status of Client {0}")
	public void validatePendingDocStatus(String cname)
	{
		Dashboard.verifyDocStatus("Pending", cname);
	}
	
	@Step("Validate 'Pending' Account Status of Client {0}")
	public void validatePendingAccStatus(String cname)
	{
		Dashboard.verifyAccStatus("Pending", cname);
	}

		
}
