package com.bambu.app.testcases;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import com.bambu.app.utils.Util;
import com.bambu.app.steps.AdminConsoleClientSteps;


import net.serenitybdd.jbehave.SerenityStories;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

public class AdminConsoleTest extends SerenityStories {

	@Managed(driver = "chrome")
	public static WebDriver wd;

	ChromeDriverService service = null;
	//private static final String TASKLIST = "killall";
	private static final String TASKLIST = "ps -few";
	//private static final String KILL = "killall /F /IM ";
	private static final String KILL = "TASKKILL /F /IM ";
	static String oldRegion="";
	static String newRegion="";
	
	@Steps
	AdminConsoleClientSteps AdminConsole_Client;
	
	@Given("Admin user is logged into the Admin Console login page with Username:$Username and Password:$Password")
	public void givenAdminUserIsOnLoginPage(String uname,String pwd) {
		AdminConsole_Client.onLoginPage();
		AdminConsole_Client.logintoClientList(uname,pwd);
	}	
	
	@Given("a {THAI|Foreigner} client:\"$ClientName\" pending authorization is available")
	public void givenClientPendingAuthorization(String cname) {		
		//AdminConsole_Client.searchClientThroughPages(cname);
		AdminConsole_Client.searchClient(cname);
		AdminConsole_Client.validatePendingDocStatus(cname);
		AdminConsole_Client.validatePendingAccStatus(cname);
	}
		
	@When("Admin navigates to Client Profile of {THAI|Foreigner} Client:\"$ClientName\"")
	public void andAdminNavigatestoClientProfile(String cname)
	{
		AdminConsole_Client.navigatetoClientProfile(cname);
		AdminConsole_Client.verifyClientDetailsDisplayed(cname);
	}
	
	@When("Admin approves all Documents for the THAI Client")
	public void andAdminApprovesAllDocs()
	{
		AdminConsole_Client.clickonDocuments();	
		AdminConsole_Client.approveAllThaiDocuments();
	}
	
	@When("Admin rejects all Documents for the THAI Client")
	public void andAdminRejectsAllDocs()
	{
		AdminConsole_Client.clickonDocuments();	
		AdminConsole_Client.rejectAllThaiDocuments();
	}
	
	@When("Admin approves all Documents for the Foreigner Client")
	public void andAdminApprovesAllDocsForeigner()
	{
		AdminConsole_Client.clickonDocuments();	
		AdminConsole_Client.approveAllForeignerDocuments();
	}
	
	@When("Admin rejects all Documents for the Foreigner Client")
	public void andAdminRejectsAllDocsForeigner()
	{
		AdminConsole_Client.clickonDocuments();	
		AdminConsole_Client.rejectAllForeignerDocuments();
	}
	
	@When("Admin approves Client KYC")
	public void andAdminApprovesClientKYC()
	{
		AdminConsole_Client.clickProfileTab();
		AdminConsole_Client.approveKYC();
	}
	
	@When("Admin rejects Client KYC")
	public void andAdminRejectsClientKYC()
	{
		AdminConsole_Client.clickProfileTab();
		AdminConsole_Client.rejectKYC();
	}	
		
	@Then("{THAI|Foreigner} Client:\"$ClientName\" approval should be successful")
	public void thenClientApprovalSuccess(String cname)
	{
		AdminConsole_Client.navigatetoClientsPage();
		AdminConsole_Client.validateApprovedDocStatus(cname);
		AdminConsole_Client.validateApprovedAccStatus(cname);		
	}
	
	@Then("{THAI|Foreigner} Client:\"$ClientName\" rejection should be successful")
	public void thenClientRejectionSuccess(String cname)
	{
		AdminConsole_Client.navigatetoClientsPage();
		AdminConsole_Client.validateRejectedDocStatus(cname);
		AdminConsole_Client.validateRejectedAccStatus(cname);		
	}
	

	//---------------------------------------------------------------------------------------
	@BeforeScenario
	public void Check_Chrome() throws Exception {
		System.out.println("----Before Test------");
		
		  String processName = "chromedriver.exe"; 
		/*
		 * if (isProcessRunning(processName)) { killProcess(processName); } String
		 * processName2 = "chrome.exe"; if(isProcessRunning(processName2)) {
		 * killProcess(processName2); }
		 */
		 
		try {
			File chromeDriverLogFile = new File(System.getProperty("user.dir") + "/chromedriver.logs");
			if (chromeDriverLogFile.exists()) {
				chromeDriverLogFile.delete();
			}

			
			  service = new ChromeDriverService.Builder() .usingDriverExecutable(new
			  File(Util.get().getSerenityProperty("webdriver.chrome.driver")))
			  .usingPort(9515).withVerbose(true) .withLogFile(new
			  File(System.getProperty("user.dir") + "/chromedriver.logs")).build();
			  service.start();
			 
			 
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error while launching the new ChromeDriverService", e);
		}
		System.out.println("chromeDriver Service started on : " + service.getUrl());

	}

	@AfterScenario
	public void StopChromeService() throws Exception {

		System.out.println("-----After Test------");
		wd.close();
		wd.quit();
		
		   service.stop(); String processName = "chromedriver.exe"; 
		/*
		 * if (isProcessRunning(processName)) {
		 * 
		 * // log_out();
		 * 
		 * killProcess(processName); } String processName2 = "chrome.exe"; if
		 * (isProcessRunning(processName2)) { killProcess(processName2); }
		 */
		 

	}

	public static boolean isProcessRunning(String serviceName) throws Exception {
		Process p = Runtime.getRuntime().exec(TASKLIST);
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			// System.out.println(line);
			if (line.contains(serviceName)) {
				return true;
			}
		}
		return false;
	}

	public static void killProcess(String serviceName) throws Exception {
		System.out.println("Inside Kill Process -- >"+serviceName);
		Runtime.getRuntime().exec(KILL + serviceName);
	}
	
}
