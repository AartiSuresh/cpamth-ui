Admin console Client Functionality

Meta:
@AdminConsole_Client
Narrative:
In order to approve or reject a Client
As an Admin
I want to first approve or reject the Documents and Client KYC


Scenario: TC1. Approval of THAI Client workflow
Meta:
@norun
Given Admin user is logged into the Admin Console login page with Username:test888@test.com and Password:12345678C
And a THAI client:"Prakrutii" pending authorization is available
When Admin navigates to Client Profile of THAI Client:"Prakrutii"
And Admin approves all Documents for the THAI Client
And Admin approves Client KYC
Then THAI Client:"Prakrutii" approval should be successful

Scenario: TC2. Approval of Foreigner Client workflow
Meta:
@norun
@Pending
Given Admin user is logged into the Admin Console login page with Username:test888@test.com and Password:12345678C
@Pending
And a Foreigner client:"Cpamtwoplustwo" pending authorization is available
@Pending
When Admin navigates to Client Profile of Foreigner Client:"Cpamtwoplustwo"
@Pending
And Admin approves all Documents for the Foreigner Client
@Pending
And Admin approves Client KYC
@Pending
Then Foreigner Client:"Cpamtwoplustwo" approval should be successful

Scenario: TC3. Rejection of THAI Client workflow
Meta:
@run
@Pending
Given Admin user is logged into the Admin Console login page with Username:test888@test.com and Password:12345678C
@Pending
And a THAI client:"Prakrutii" pending authorization is available
@Pending
When Admin navigates to Client Profile of THAI Client:"Prakrutii"
@Pending
And Admin rejects all Documents for the THAI Client
@Pending
And Admin rejects Client KYC
@Pending
Then THAI Client:"Prakrutii" rejection should be successful

Scenario: TC4. Rejection of Foreigner Client workflow
Meta:
@norun
@Pending
Given Admin user is logged into the Admin Console login page with Username:test888@test.com and Password:12345678C
@Pending
And a Foreigner client:"Cpamtwoplustwo" pending authorization is available
@Pending
When Admin navigates to Client Profile of Foreigner Client:"Cpamtwoplustwo"
@Pending
And Admin rejects all Documents for the Foreigner Client
@Pending
And Admin rejects Client KYC
@Pending
Then Foreigner Client:"Cpamtwoplustwo" rejection should be successful